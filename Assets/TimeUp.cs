﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.InfiniteRunnerEngine
{
    public class TimeUp : MonoBehaviour {

        void FixedUpdate()
        {
            if (GameManager.Instance.GameTime >= 120)
            {
                Invoke("WinGame", 0);
            }

        }
        public void WinGame()
        {
            GameManager.Instance.SetStatus(GameManager.GameStatus.GameOver);
            GUIManager.Instance.SetWinScreen(true);

        }
    }
}