﻿using UnityEngine;
using System.Collections;
namespace Assets.InfiniteRunnerEngine.Scripts.MovingObjects
{
    public class ReactivateOnSpawn : MonoBehaviour
    {
        public bool ShouldReactivate = true;

        public virtual void Reactivate()
        {
            if (ShouldReactivate)
            {
                gameObject.SetActive(true);
            }
        }
    }
}
