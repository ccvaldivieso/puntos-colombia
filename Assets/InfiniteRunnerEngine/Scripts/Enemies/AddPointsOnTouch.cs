﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace MoreMountains.InfiniteRunnerEngine
{	
	/// <summary>
	/// Add this class to a trigger boxCollider and it'll kill all playable characters that collide with it.
	/// </summary>
	public class AddPointsOnTouch : MonoBehaviour 
	{
        string[] Messages = {
            "Los puntos se acumularan muy rapido, porque siempre tendras un aliado Puntos Colombia cerca de ti",
            "Cines, restaurantes, moda, gasolina, mercado, diversion. muchas categorias para que ganes puntos por vivir",
            "Por comprar en nuestras marcas con cualquier medio de pago, ganaras Puntos Colombia, y por pagar con las Tarjetas de Credito Bancolombia, ganaras muchos mas",
            "Quieres conocer todas nuestras marcas Entra a www.puntoscolombia.com"};

        string[] MessagesCoin = {
            "Mas de 300 aerolineas y 300 mil hoteles alrededor del mundo te esperan con Puntos Colombia. ",
            "Ganaras puntos todo el tiempo para redimirlos en lo que te gusta",
            "Puntos Colombia, El programa en el que las marcas que acompañan tu dia a dia, estaran listas para premiar tus compras",
            "Tiquetes de avion, reservas de hotel, planes de vacaciones y mas, vivelos con Puntos Colombia"};

        [Header("Sonido")]
        public AudioSource audiocoin;
        [Header("Aliado")]
        public bool aliado = false;
        [Header("Coin Especial")]
        public bool coinEspecial = false;
        /// the points to add
        [Header("Points")]
        public int PointsToAdd = 10;
        /// <summary>
        /// Handles the collision if we're in 2D mode
        /// </summary>
        /// <param name="other">the Collider2D that collides with our object</param>
        protected virtual void OnTriggerEnter2D (Collider2D other)
		{
			TriggerEnter (other.gameObject);
		}
		
		/// <summary>
		/// Handles the collision if we're in 3D mode
		/// </summary>
		/// <param name="other">the Collider that collides with our object</param>
	    protected virtual void OnTriggerEnter (Collider other)
		{		
			TriggerEnter (other.gameObject);
		}	
		
		/// <summary>
		/// Triggered when we collide with either a 2D or 3D collider
		/// </summary>
		/// <param name="collidingObject">Colliding object.</param>
		protected virtual void TriggerEnter(GameObject collidingObject)
		{
			// we verify that the colliding object is a PlayableCharacter with the Player tag. If not, we do nothing.			
			if (collidingObject.tag!="Player") { return; }	

			PlayableCharacter player = collidingObject.GetComponent<PlayableCharacter>();
			if (player==null) { return; }	

			// we ask the LevelManager to kill the character
			//LevelManager.Instance.KillCharacter(player);
            //LevelManager.Instance.KillCharacter(player);
            GameManager.Instance.AddPoints(PointsToAdd);
            if (aliado) {
                //GameManager.Instance.ShowInfo(); //Muesta el Mensaje del Aliado
                //MensajesText.text = FraseRes(Messages);
                GUIManager.Instance.TipEspecial.SetActive(false);
                GUIManager.Instance.MensajesCoin.text = "";
                GUIManager.Instance.Mensajes.text = FraseRes(Messages);
                GameManager.Instance.Pause();

            }
            else
            {
                audiocoin.PlayDelayed(0);
            }
            if (coinEspecial)
            {
                GUIManager.Instance.TipEspecial.SetActive(true);
                GUIManager.Instance.Mensajes.text = "";
                GUIManager.Instance.MensajesCoin.text = FraseRes(MessagesCoin);
                GameManager.Instance.Pause();

            }


        }

        string FraseRes(string[] Messages_)
        {
            System.Random random = new System.Random();
            int randIndex = random.Next(Messages_.Length);
            string returnMessage = Messages_[randIndex];
            //print(randIndex);
            return returnMessage;
        }
    }

}