﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RandomText : MonoBehaviour {
    public Text MensajesText;
    // Use this for initialization
    //void Start () {
    //    string[] Tipos = {
    //        "Los puntos se acumularán muy rápido, porque siempre tendrás un aliado Puntos Colombia cerca de ti",
    //        "Cines, restaurantes, moda, gasolina, mercado, diversión…¡muchas categorías para que ganes puntos por vivir!",
    //        "Por comprar en nuestras marcas con cualquier medio de pago, ganarás Puntos Colombia, y por pagar con las Tarjetas de Crédito Bancolombia, ¡ganarás muchos más!",
    //        "¿Quieres conocer todas nuestras marcas? Entra a www.puntoscolombia.com"};
    //    MensajesText.text = FraseRes(Tipos);

    //}

    string FraseRes(string[] Messages_)
    {
        System.Random random = new System.Random();
        int randIndex = random.Next(Messages_.Length);
        string returnMessage = Messages_[randIndex];
        //print(randIndex);
        return returnMessage;
    }
	
	// Update is called once per frame
	void Update () {
        print("updating the messageText :)");
        string[] Messages = {
            "Los puntos se acumularán muy rápido, porque siempre tendrás un aliado Puntos Colombia cerca de ti",
            "Cines, restaurantes, moda, gasolina, mercado, diversion ¡muchas categorías para que ganes puntos por vivir!",
            "Por comprar en nuestras marcas con cualquier medio de pago, ganarás Puntos Colombia, y por pagar con las Tarjetas de Crédito Bancolombia, ¡ganarás muchos más!",
            "Quieres conocer todas nuestras marcas Entra a www.puntoscolombia.com"};
        MensajesText.text = FraseRes(Messages);
    }
}
